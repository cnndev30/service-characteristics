
```mermaid
classDiagram

Resource o-- Resource
Provider "1" --> "*" Resource : operates
ServiceOffering o-- Resource
Resource <|-- DataResource
Resource <|-- SoftwareResource
Resource <|-- Node
Resource <|-- Interconnection
Resource <|-- NetworkingDevice

```
