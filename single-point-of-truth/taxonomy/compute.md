```mermaid
classDiagram

Compute <|-- BareMetal
Compute <|-- VirtualMachine
Compute <|-- Function
Compute <|-- Container
Compute <|-- Registry

```
