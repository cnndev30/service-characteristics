```mermaid
classDiagram

class Interconnection {
    <<abstract>>
}

PhysicalResource <|-- PhysicalInterconnection

Interconnection <|-- PhysicalInterconnection
Interconnection <|-- VirtualInterconnection

Interconnection <|-- PeeringService
Interconnection <|-- PrivateInterconnectionService

InstantiatedVirtualResource <|-- VirtualInterconnection
```
