# Yaml Files

Single Point of Truth of Self-Description Schema. [See Governance Process](../documentation/governance) for more details.

## YAML Validation

All yaml files will be validated at every merge request. There is a syntactical check by checking *.yaml files against [json schema](single-point-of-truth/yaml/validation/schema.json) and there is a semantical check done by [check_yaml.py](toolchain/check_yaml.py). 

**NOTE: Any changes to this list have to result in an update to the validation script (check_yaml.py)**

## Guidelines for writing valid YAML files

* Use one file per class in taxonomy

* Yaml file name must start with lower case character. File name must be equal to name of class in Gaia-X Conceptual Model, e.g. `provider.yaml` for class `Provider`. If name in Conceptual Model is a composed one, such as `Service Offering`, spaces are replace by dashes and all strings start with lower case character each. E.g.yaml file for `Service Offering` should be called `service-offering.yaml`.

* First element of each yaml file must be the name of a class in Self Description taxonomy using CamelCase notion. E.g. Service Offering is translated to ServiceOffering.

* Key `subClassOf` is mandatory for a `class`. If `subClassOf` is set, value must be a list with super classes in upper CamelCase notation. For each list item in `subClassOf` an additional yaml file must exist. If class has no 'subClassOf' use an empty list. 

* Key `abstract` is optional and indicated that his class is not allowed for instantiation.

* Key `attributes` is mandatory for a `class`. It must be a list of attribute. Each attribute has to consist of six mandatory keys (`title`,`prefix`,`dataType`, `cardinality`, `descripton`,`exampleValues`). 

* Value of `title` is mandatory for an `attribute`. It must be unique within each file as it is used as ID of a Self-Description attribute.

* Value of `prefix` is mandatory for an `attribute`. It must be an abbreviation defined in [prefixes.yaml](validation/prefixes.yaml).  
It is needed to define the origin of the attribute to correctly generate subsequent files. 

* Value of `dataType` is mandatory for an `attribute`. It must be an abbreviation listed in [dataTypeAbbreviation.yaml](validation/dataTypeAbbreviation.yaml). Mapping of abbreviation in `dataTypeAbbreviation.yaml` must be a valid data type in Self-Description ontology.

* Key `cardinality` is mandatory for an `attribute`. It is represented by a string of the form 'minCount..maxCount'. E.g., if an attribute has to be defined at least once and at most 5 times the correct string would be '1..5'. It is important that the value of the minCount and maxCount are seperated by exactly '..' in the string. If there is no minCount the proper value is '0', and if there is no maxCount the proper value is '\*'. So the cardinality string describing that an attribute has no minCount and no maxCount restrictions would be '0..*'.

* Value of `minCount` and `maxCount` must be a positive integer

* Value of `descripton` is mandatory for an `attribute`. It must be at least one human readable sentence.

* Value of `exampleValues` is mandatory for an `attribute`. It must be a list. There must be at least one list item.

Note: Relationships between classes are modeled as (non-) mandatory attributes, too.

## Developer Support

The schema of the yaml files is formally described as JSON Schema in the 'schema.json' file. In the future this file might be publicly available, so it can be referenced directly.
This can be loaded into any IDE to get auto-completion.

* For VSCode: Install the 'YAML'-Extension. In your users settings add:

    ```text
        "yaml.schemas": {
            "./yaml/validation/schema.json": ["/yaml/*"]
        },
    ```

* For IntelliJ: Add the schema.json to the Schema Mappings

If you've installed Docker, you can run

```bash
docker run --rm -v ${PWD}:/yaml 3scale/ajv test -s /yaml/validation/schema.json -d /yaml/<YourYaml>.yaml  --valid
```

inside the yaml-folder to validate your file. To check all yaml files

```bash
 for f in ./*.yaml; do docker run --rm -v ${PWD}:/yaml 3scale/ajv test -s /yaml/validation/schema.json -d /yaml/$f --valid; done
```

