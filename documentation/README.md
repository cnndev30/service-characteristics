# Documentation

This folder contains all documentation related to Self-Description, divided into the following parts:

- [Self-Description Document](sd-document): Documentation about Self-Description Schema
- [Self-Description Tutorial](sd-tutorials): Tutorial how to write your own Self-Descriptions
- [Governance Process](governance-process.md): Documentation of the governance process of Self-Descriptions, including a guide on how to contribute to development of Self Description Schema.

Please refer to the appropriate folders for more details.
