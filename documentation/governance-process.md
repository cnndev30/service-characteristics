# Governance Process

## Single Point of Truth

Self-Descriptions schema consists of a

- **Self-Description Taxonomy**: defines all entities (= classes) that can be described. All classes inherit from one of the three top-level classes defined in [Gaia-X Conceptual Model](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-architecture-document/-/blob/master/architecture_document/conceptual_model.md), i.e., Participant, Service Offering, and Resource.
- **A set of attributes**: available to describe the properties of each entity (= class)

Self-Description Taxonomy and attributes are stored as YAML files as Single-Point-of-Truth (SPoT) in [yaml](/yaml) directory. 

Based on our SPoT, we generate all artifacts related to Self-Description and their schemata automatically. We support the following artifacts:

- **Self-Description Ontology**: All classes as well as their properties (attributes and relationships) are available to describe entities in Gaia-X. We use a subset of the W3C [RDF Schema](https://www.w3.org/TR/rdf-schema/) and [Web Ontology Language OWL](https://www.w3.org/TR/owl2-overview/) for these descriptions, and the [RDF Turtle](https://www.w3.org/TR/turtle/) serialization format.
- **Self-Description Constraints**: Rules for the usage of elements and properties defined in the Self-Description Ontology. These rules, implemented as SHACL shapes, are also used to verify whether or not a Self-Description is syntactical valid. 
- **End user documentation**: User-friendly, HTML based documentation on self-description schema.

```mermaid
flowchart LR
yaml(YAML files) --auto-generated--> onto("Self-Description Ontology\n (Turtle)") --auto-generated--> doc("Self-Description Docu\n (HTML)") 
yaml --auto-generated--> const("Self-Description Constraints\n (SHACL shapes)") --auto-generated--> doc 
```

## How to contribute

We have four main working streams, to which you are welcome to contribute. These working streams are:

- Self-Description schema: Extending taxonomy and attibutes (YAML files as single point of truth).
- Documentation: Documentation of the Self-Description schemata, tutorials as well as dissemination (landing page). 
- Tool chain: End user tools to create, manage, validate and use Self-Descriptions.
- CI/CD Pipeline: Scripts to check SPoT and to generarte  

If you want to contribute to one or more of the work streams listed above, please follow the following process.

#### How to contribute to SD development

- Create a new issue
- Add appropriate label(s) to your issue. Tag issue status, especially use `status:in_progress` and `status:ready_for_review`. 
- Create a new branch with your changes. Name of branch should start with the number of the issue, e.g. *195-governance-process* for issue [#195](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/issues/195)
- Create a new merge request to merge your branch to target branch `develop`
- Join our Weekly Meeting to present your request for change to the Sub-Working Group's members and discuss it with them.
- Answer and resolve threads in your Merge Request. Note: Thread may be re-opened by other members. 

**Guidelines for merge into develop**:

+ No unresolved thread, no merge conflicts
+ Successfully run CI pipeline
+ Presentation of changes in weekly SD meeting. Members will vote for change. Simple majority of attendees is sufficent.
+ Review and approval of one member of the Self-Description community. **Note: Only merge requests with label `status:ready_for_review` will be approved and merged.**

In case your merge request was approved and your changes are relevant for the Gaia-X Architecture Document or other Gaia-X deliverables:

- Create and Request for Change (RfC)/Architecture Decision Record (ADR) in [Gaia-X Architecture Repository](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-architecture-document) to propagate your changes back.

**PLEASE NOTE: The author, this means you, is responsible for propagating relevant changes back to Gaia-X deliverables. This will not happen automatically or by any other member of the community.**
  

#### Providing Self-Description sample instances

- Follow the steps described in [`ci/LICENSE_ACCEPTED_BY.txt`](ci/LICENSE_ACCEPTED_BY.txt)

- Create a new Branch containing your samples 
- Create a new Merge Request to merge your branch to target branch `develop`

   **-** **PLEASE NOTE: ALL SD CONTENTS MIGHT BE SHARED PUBLICLY - DO NOT ADD ANY PRIVACY- OR SECURITY-CRITICAL OR CONFIDENTIAL DETAILS**


## Release Cycles

The specification of Self-Descriptions is part of the Gaia-X Architecture Document (AD), which is released every three to six months. The Self-Description release cycle matches the AD release cycle. For each AD release, we will have a corresponding Milestone that collects issues to be addressed and that monitors progress.

Every Self-Description release will start with a *release planning phase*, identifying issues to be addressed within the next three to six months. Every release will end with a short *retrospective*, looking back at the work that was done. This time may also be used to critically evaluate our processes and performance.


