# Tools for Self-Description

We divide the toolchain into end-user tools. The toolchain supports CI/CD Pipeline. End user tools are for users to create and manage self-descriptions. We support the following end user tools: 

- Creation Wizard
- Validation Wizard
- Visualization Wizard


## End User Tool

### Creation Wizard


### Visualitation Wizard


### Validation Wizard


## Tool Chain

### Generation of Widoco Documentation

The documentation of all underlying ontology modules ([core](https://www.w3id.org/gaia-x/core), [contract](https://www.w3id.org/gaia-x/contract), [node](https://www.w3id.org/gaia-x/node), [participant](https://www.w3id.org/gaia-x/participant), [resource](https://www.w3id.org/gaia-x/resource), and [service offering](https://www.w3id.org/gaia-x/service)) is fully-automatically generated using [WIDOCO](https://github.com/dgarijo/Widoco). It takes the ontology file(s) as input and creates an html file containing the ontology information and including a visualization using [WebVOWL](vowl.visualdataweb.org/webvowl.html). 

An exemplary standalone command-line execution could be:

`java -jar widoco.jar -ontFile ./implementation/ontology/Contract/Contract.ttl -outFolder docContract -webVowl -uniteSections -rewriteAll`

For additional information on the parameters please take a look at the [WIDOCO documentation](https://github.com/dgarijo/Widoco).

See also: [/toolchain/docgen](/toolchain/docgen)


### Semantic validation of *.yaml files

check_yaml.py validates all files [/yaml/]()
Source: check_yaml.py

### Generation of Json files for validation of self descriptions

```shell
python3 toolchain/yaml2json.py 
```


Reads out all yaml files in "yaml/*.yaml" and creates json (*.json) files that can be used to validate self-descriptions.\
Examples of the generated files can be found in implementation/validation/instance_json_validation.

With each commit to the master branch, the latest version of the validation files are saved to [public/json-validation/](public/json-validation/). \
The automatically generated JSON files follow the json schema as define here: https://json-schema.org/

### Ontology Generation

```shell
python3 toolchain/ontology_generation/yaml2ttl.py 
```

Reads out all yaml files in "yaml/*.yaml" and creates Turtle (*.ttl) files that describe the ontology. 

Examples of the generated files can be found in [implementation/ontology/auto-generated](implementation/ontology/auto-generated). \
Note, these examples might not represent the latest version. \

With each commit to the master branch, the latest version of the ontology is computed. 
The (*.ttl) files are saved to [public/ontology/generated/](public/ontology/generated/).

In more detail, for each gax-prefix (e.g gax-participant) the script generates one ontology file (e.g participant_generated.ttl) that describes 
all classes associated with this prefix (e.g trustedCloudProvider.yaml', 'participant.yaml', 'provider.yaml').
 - Each ontology file starts with a list of prefixes that are used within the ontology file.
   The list of prefixes that are included is defined in [toolchain\ontology_generation\prefixes.yaml](toolchain\ontology_generation\prefixes.yaml)
 - Each ontology file includes a definition of the associated gax-prefix. This information is not included in the *.yaml files. 
   Thus, we utilize other files to define this definition. They are located in [toolchain\ontology_generation\prefix_definitions](toolchain\ontology_generation\prefix_definitions) 
 - Each gax-prefix definition has a property dct:modified described as a xsd:dateTimeStamp, which is set to the execution time of the script.
 - Each ontology file includes a definition of classes (owl:Class). Each owl:Class is defined by one *.yaml file. 
 - Each definition of a class  includes the class definition itself and a definition of its associated DatatypeProperties (owl:DatatypeProperty).


### Creating the SHACL shapes
The 'createShacl' job of the [Gitlab CI pipeline](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/blob/master/.gitlab-ci.yml) automatically creates a SHACL shape for each of the classes defined by the given [YAML files](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/tree/master/yaml). These shapes are *.ttl files and each created shape is named as the YAML file it is based on with an extending "Shape" to the name. E.g.:

Based on the "participant.yaml" file a "participantShape.ttl" file is created.

These SHACL shapes are used to validate the correctness of Self Descriptions uploaded to the repository. The creation of the SHACL files is done by a [python script](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/blob/master/toolchain/yaml2shacl.py) and the resulting files are stored in the /public/validation/ directory.

### SD Attribute Generator

Reads out yaml files and conceptual model files and concatenates both to a collection of tables with mandatory and optional attributes for each class of Self Description Schema. Tables are written in markdown and places in [/documentation/auto-generated](/documentation/auto-generated)

```mermaid
graph TD
    yaml(yaml Files) -->|e.g provider.yaml| sd_tables(SD Attributes)
    concept(Conceptual Model Files) -->|e.g provider.md| sd_tables(SD Attributes Markdown File)
```

To generate list of attribute tables run

```shell
python3 toolchain/docgen/cli.py sd-attributes --srcYaml yaml --srcConcept conceptual-model --fileName documentation/auto-generated/sd-attributes.md

```

### Validation
The validation of the self descriptions uploaded to this repository is done using [SHACL](https://www.w3.org/TR/shacl/) shapes. These can be found in the [validation directory](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/tree/master/implementation/validation).

The [ci-pipeline](../.gitlab-ci.yml) calls [a tailored python script](./check_shacl.py) which uses [pySHACL](https://github.com/RDFLib/pySHACL) to validate all Self Descriptions against defined SHACL shapes ("schemas"). A standalone command-line execution could be:

`pyshacl -s pathToSHACLFile/shape.ttl -m -i rdfs -a -j -f human pathToSDFile/sd.jsonld`

For additional information on the parameters, please refer to the [pySHACL documentation](https://github.com/RDFLib/pySHACL).
### Visualization


### Constraints

The visualization of the constraints (e.g., mandatory and optional attributes) defined in the SHACL shapes can be found [here](https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/constraints.html). It is created using the tool [shacl-play](https://shacl-play.sparna.fr/play/), which generates UML diagrams from SHACL shapes. 

An exemplary standalone command-line execution could be:

`java -jar shacl-play.jar draw -i implementation/validation/DistributionShape.ttl -o public/constraints/DistributionShape.png`

It takes an SHACL shape as input and generates a PNG File showing the corresponding UML diagram as output.

### Instances
The visualization for JSON-LD instances are available under https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/visualization/instances/.
The exact path to a specific instances is defined by the [location of the instance](../implementation/instances/) inside the repository.
For example, the [ACME-Provider](../implementation/instances/provider/ACME.jsonld) is visualized [here](https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/visualization/instances/provider/ACME/index.html).

To generate the visualization locally, a Node.js environment is required.
[This script](../toolchain/visualization/build.sh) generates the visualization for all instances.




