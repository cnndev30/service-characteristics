#!/usr/bin/env python3
import os
import pyshacl
import rdflib
from colorama import Fore, Style
import glob

def mergeDataGraphs():
    graph = rdflib.Graph()
    for filename in glob.glob('../implementation/instances/*/*.jsonld'):
        try:
            graph.parse(filename, format='json-ld')
        except Exception as e:
            raise Exception("An error occurred while parsing " + str(filename) + ": " + str(e))

    graph.serialize('mergedDataGraph.ttl', format='ttl')

def validation():

    err_count = 0
    for filename in glob.glob('../yaml2shacl/*.ttl'):
        print(filename)
        r = pyshacl.validate('mergedDataGraph.ttl', shacl_graph=filename)
        conforms, results_graph, results_text = r
        if not conforms:
            print(results_text)
            err_count += 1
    for filename in glob.glob('../single-point-of-truth/shacl/*.ttl'):
        print(filename)
        r = pyshacl.validate('mergedDataGraph.ttl', shacl_graph=filename)
        conforms, results_graph, results_text = r
        if not conforms:
            print(results_text)
            err_count += 1
    return err_count


if __name__ == '__main__':

    mergeDataGraphs()
    err_count = validation()

    if err_count > 0:
        print(Style.BRIGHT + Fore.RED + 'found {} error(s)'.format(err_count))
    os.sys.exit(err_count)

