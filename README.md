# GAIA-X Service Characteristics

This repository contains documentation and implementation on GAIA-X Self-Descriptions (SDs).

If you want to learn more about Self Descriptions and their schemata, please inspect the [Self-Description Document](documentation/sd-document).

If you need help writing your own Self-Descriptions, please read the [Self-Description Tutorial](documentation/sd-tutorial).

If you want to know how to contribute to any decision making for Self-Descriptions, please refer to our [Governance Process](documentation/governance-process).

## GitLab Structure

Folders:
- `single-point-of-truth`: Single point of truth with respect to Self-Description Schema. Schema contains of a hirarchy of classes, called taxonomy and a set of attributes for each class. Attributes are encoded in YAML files. 
- `documentation`: All documentation regarding SDs.
- `implementation`: All SD-related vocabularies, templates, examples, shapes/rules, and queries.
- `toolchain`: Toolchain facilitating creation, validation and visualization of Self-Descriptions as well as scripts of our CI/CD pipeline.


## Organization

- All communication is done via Mailing List. Please use the [Gaia-X Onboarding formula](https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame&formId=F-kxJG6whD) to join our group.
- We meet weekly on Friday 09:00 - 10:00 (CEST)
- Sources:
  - [GitLab](https://gitlab.com/gaia-x/technical-committee/service-characteristics/): source code
  - [NextCloud](https://community.gaia-x.eu/f/14659414): binary files
- Lead: Christoph Lange-Bever, Vice-Lead: Anja Strunk


## Landing Page

This repository is for WG members only. 
The output is published here https://gaia-x.gitlab.io/technical-committee/service-characteristics/
